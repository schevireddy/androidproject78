package com.savendeveloper.project78.listners;

public interface Listener {

    void onDialogDisplayed();
    void onDialogDismissed();
    void onNfcFound();
}
