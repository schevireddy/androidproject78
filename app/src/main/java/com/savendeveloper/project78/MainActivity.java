package com.savendeveloper.project78;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.savendeveloper.project78.networklayer.APIClient;
import com.savendeveloper.project78.networklayer.RequestBody;
import com.savendeveloper.project78.networklayer.ServiceInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {


    private EditText tf_name,tf_email,tf_phone,tf_password;
    private Spinner tf_account;
    private Button btn_sendData;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        tf_name = findViewById(R.id.name);
        tf_email = findViewById(R.id.email);
        tf_phone = findViewById(R.id.phone);
        tf_password = findViewById(R.id.password);
        tf_account = findViewById(R.id.accountSetting);
        btn_sendData = findViewById(R.id.button);
        progressBar = findViewById(R.id.progressBar);

        btn_sendData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                action_post();
            }
        });

        ArrayAdapter<String> ary_Items = new ArrayAdapter<String>(MainActivity.this,android.R.layout.simple_list_item_1,getResources().getStringArray(R.array.ary_accountSettings));
        ary_Items.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        tf_account.setAdapter(ary_Items);


    }


    private void action_post(){

        progressBar.setVisibility(View.VISIBLE);

        ServiceInterface apiInterface = APIClient.getClient().create(ServiceInterface.class);
        final RequestBody requestBody = new RequestBody(tf_name.getText().toString(),
                tf_email.getText().toString(),
                tf_phone.getText().toString(),
                tf_password.getText().toString(),
                tf_account.getSelectedItem().toString());

        // Network Call
        Call<String> postRequest = apiInterface.postTheForm(requestBody);
        postRequest.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, final Response<String> response) {
                Log.d("Response",response.body());
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.INVISIBLE);
                        Toast.makeText(MainActivity.this,response.body(),Toast.LENGTH_LONG).show();

                        // Launch the NFC Activity Screen
                        Intent nfcIntent = new Intent(MainActivity.this,NFCActivity.class);
                        nfcIntent.putExtra("postedBundle",requestBody);
                        startActivity(nfcIntent);
                    }
                });
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.INVISIBLE);
                        Toast.makeText(MainActivity.this,"Failed",Toast.LENGTH_LONG).show();
                    }
                });

            }
        });



    }


}
