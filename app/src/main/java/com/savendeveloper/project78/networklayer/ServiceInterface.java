package com.savendeveloper.project78.networklayer;

import com.savendeveloper.project78.networklayer.response.Response;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ServiceInterface {

    /**
     * To add the user to the Database.
     * @param requestBody
     * @return
     */
    @POST("DelPharmCreateUser/AddUser")
    Call<String> postTheForm(@Body RequestBody requestBody);

    /**
     * Get the status of the User depending on NFC status.
     * @param emailAddress
     * @return
     */
    @GET("DelPharmAccess")
    Call<String> getTheResponseForNFC(@Query("email") String emailAddress);

    /**
     * To post the status of `primaryKey` => `email` to ESP8266 chip.
     */
     @GET("Solenoid={solenoidStatus}")
    Call<String> getTheSolenoidResponse(@Path("solenoidStatus") String status);

}
