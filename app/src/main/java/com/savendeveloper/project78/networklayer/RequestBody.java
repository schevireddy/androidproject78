package com.savendeveloper.project78.networklayer;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RequestBody implements Serializable {
    @SerializedName("Name")
    private String Name;

    @SerializedName("Email")
    private String Email;

    @SerializedName("PhoneNumber")
    private String PhoneNumber;

    @SerializedName("Password")
    private String Password;

    @SerializedName("AccountSetting")
    private String AccountSetting;

    public RequestBody(String name, String email, String phoneNumber, String password, String accountSetting) {
        Name = name;
        Email = email;
        PhoneNumber = phoneNumber;
        Password = password;
        AccountSetting = accountSetting;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getAccountSetting() {
        return AccountSetting;
    }

    public void setAccountSetting(String accountSetting) {
        AccountSetting = accountSetting;
    }
}
