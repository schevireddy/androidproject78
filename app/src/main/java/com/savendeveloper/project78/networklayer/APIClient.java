package com.savendeveloper.project78.networklayer;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIClient {
    private static OkHttpClient okClient() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.connectTimeout(1, TimeUnit.HOURS);
        httpClient.readTimeout(1,TimeUnit.HOURS);
        httpClient.writeTimeout(1,TimeUnit.HOURS);
        return httpClient.build();
    }

    public static Retrofit getClient() {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
    public static Retrofit getAndroidClient(){
        return new Retrofit.Builder()
                .baseUrl(solenoidUrl)
                .client(okClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    private static String baseUrl = "https://delpharmapi.azurewebsites.net/api/";
    private static String solenoidUrl = "https://192.168.1.70/";

}
