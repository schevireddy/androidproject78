package com.savendeveloper.project78;

import androidx.appcompat.app.AppCompatActivity;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.savendeveloper.project78.fragments.NFCReadFragment;
import com.savendeveloper.project78.listners.Listener;
import com.savendeveloper.project78.networklayer.APIClient;
import com.savendeveloper.project78.networklayer.RequestBody;
import com.savendeveloper.project78.networklayer.ServiceInterface;

import java.nio.channels.InterruptedByTimeoutException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NFCActivity extends AppCompatActivity implements Listener {

    private NfcAdapter nfcAdapter;
    private PendingIntent pendingIntent;
    private RequestBody requestBody;

    private TextView tf_name,tf_email,tf_phone,tf_accountSetting;
    private Button btn_scanNfc;
    private ProgressBar progressBar;

    //private NFCWriteFragment mNfcWriteFragment;
    private NFCReadFragment mNfcReadFragment;
    private boolean isDialogDisplayed = false;
    private boolean isWrite = false;
    private Thread nfcThread,valveThread;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nfc);

        nfcAdapter = NfcAdapter.getDefaultAdapter(this);

        // Initialize the UI Components
        tf_name = findViewById(R.id.name);
        tf_email = findViewById(R.id.email);
        tf_phone = findViewById(R.id.phone);
        tf_accountSetting = findViewById(R.id.accountSetting);

        btn_scanNfc = findViewById(R.id.action_nfc);
        progressBar = findViewById(R.id.progressBar2);

       if(getIntent().getSerializableExtra("postedBundle") != null){
           requestBody = (RequestBody) getIntent().getSerializableExtra("postedBundle");

           // If request body is not null. Load the Text View with `text`.
           tf_name.setText("Name:\n" + requestBody.getName());
           tf_email.setText("Email:\n" + requestBody.getEmail());
           tf_phone.setText("Phone:\n" + requestBody.getPhoneNumber());
           tf_accountSetting.setText("Account Setting:\n" + requestBody.getAccountSetting());
       }

        // If NFC not available on your device.
        if(nfcAdapter == null){
            Toast.makeText(NFCActivity.this,"No NFC reader found on this device.",Toast.LENGTH_LONG).show();
        }

        // If available proceed further to read the NFC.
        pendingIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, this.getClass())
                        .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);

        // Read from NFC Listner
        btn_scanNfc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    Log.d("NFC","Opened the Scanner in read mode!");
                    showReadFragment();
            }
        });

        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Create the Intent Filter for NFC Tags..
        IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
        IntentFilter ndefDetected = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
        IntentFilter techDetected = new IntentFilter(NfcAdapter.ACTION_TECH_DISCOVERED);
        IntentFilter[] nfcIntentFilter = new IntentFilter[]{techDetected,tagDetected,ndefDetected};

        // Create a pending Intent and Launch the NFC Fragment!
        PendingIntent pendingIntent = PendingIntent.getActivity(
                this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        if(nfcAdapter!= null)
            nfcAdapter.enableForegroundDispatch(this, pendingIntent, nfcIntentFilter, null);

    }

    /**
     *  Triggered when an Intent is Launched. Typecasting the intent as NFC intent using Ndef.
     */
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);

        Log.d("BODY", "onNewIntent: "+intent.getAction());

        if(tag != null) {
            Toast.makeText(this, "NFC Tag detected!", Toast.LENGTH_SHORT).show();
            Ndef ndef = Ndef.get(tag);

            if (isDialogDisplayed) {

                if (isWrite) {
                    String messageToWrite = "Saven Technologies, INC";
                    //mNfcWriteFragment = (NFCWriteFragment) getFragmentManager().findFragmentByTag(NFCWriteFragment.TAG);
                    //mNfcWriteFragment.onNfcDetected(ndef,messageToWrite);
                } else {
                    mNfcReadFragment = (NFCReadFragment)getFragmentManager().findFragmentByTag(NFCReadFragment.TAG);
                    mNfcReadFragment.onNfcDetected(ndef);
                }
            }
        }
    }


    /**
     * NOT REQUIRED for existing Code Pattern.
     * Opens the Wireless settings of the Android Beam.
     * Call this when NFC is not enabled by default.
     */
    private void showWirelessSettings(){
        Toast.makeText(NFCActivity.this,"Please Enable NFC",Toast.LENGTH_LONG).show();
        Intent wirelessIntent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
        startActivity(wirelessIntent);
    }

    /**
     * Shows the NFC Reader Fragment
     */
    private void showReadFragment() {
        mNfcReadFragment = (NFCReadFragment) getFragmentManager().findFragmentByTag(NFCReadFragment.TAG);

        if (mNfcReadFragment == null) {

            mNfcReadFragment = NFCReadFragment.newInstance();
        }
        mNfcReadFragment.show(getFragmentManager(),NFCReadFragment.TAG);

    }

    @Override
    protected void onPause() {
        super.onPause();

        // Disable the foreground dispatcher when application is paused on `nfcAdapter`.
        if (null != nfcAdapter){
            nfcAdapter.disableForegroundDispatch(NFCActivity.this);
        }
    }

    /**
     * Listener Interface methods.
     * These are `required` methods in the interface after loading the arguments in NFC Fragment
     */
    @Override
    public void onDialogDisplayed() {
        isDialogDisplayed = true;
        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onDialogDismissed() {
            isDialogDisplayed = false;
            isWrite = false;
    }

    @Override
    public void onNfcFound() {
        // Triggering the service call when NFC card is found!
        action_getResponseFromServer();
    }

    /**
     * Service Call to get Response from Server!
     */
    private void action_getResponseFromServer(){
        progressBar.setVisibility(View.VISIBLE);
        ServiceInterface apiInterface = APIClient.getClient().create(ServiceInterface.class);
        Call<String> getResponse = apiInterface.getTheResponseForNFC(requestBody.getEmail());
        getResponse.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, retrofit2.Response<String> response) {
                if(response.code() == 200){

                    // Response contains the state at which Solenoid valve should be; `0` or `1`.
                   if(null != response.body()){
                       String str_statusOfSolenoid = response.body();

                       // Check the status of the Solenoid
                       if(str_statusOfSolenoid.equals("0")){ // GET the Solenoid OFF.
                           serviceCallToSolenoidValve("OFF");
                       }else if(str_statusOfSolenoid.equals("1")){ // GET the Solenoid ON.
                           serviceCallToSolenoidValve("ON");
                       }else{
                           Log.d("STATUS","Card not detected");
                           Toast.makeText(NFCActivity.this,"Solenoid not available",Toast.LENGTH_SHORT).show();
                       }
                   }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(NFCActivity.this,"Failed to get Response from Server",Toast.LENGTH_LONG).show();
            }
        });
    }


    /**
     * Service Call to Toggle the SOLENOID.
     */
    private void serviceCallToSolenoidValve(String str_status){
        ServiceInterface serviceInterface = APIClient.getAndroidClient().create(ServiceInterface.class);
        Call<String> request = serviceInterface.getTheSolenoidResponse(str_status);
        request.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                // The linkage for Opening or Closing the gate goes here.
                Log.d("STATUS","Open or Close the Gate!");
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
             Log.d("STATUS","Developer's fault!");
            }
        });
    }



}
